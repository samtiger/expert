﻿namespace ExpertSystem.Models
{
    public class SubmitModel
    {
        #region Patient
        public string PatientName { get; set; }
        public int PatientAge { get; set; }
        #endregion

        #region Syndromes
        public string[] Syndromes { get; set; }
        #endregion

        public int DiagnosisId { get; set; }
        public string DiagnosisName { get; set; }
        public string DiagnosisDescription { get; set; }
        public string DiagnosisTreatment { get; set; }
    }
}