﻿using ExpertSystem.Entities;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ExpertSystem.Models
{
    public class DiagnosisResult
    {
        public DiagnosisResult()
        {

        }
        public DiagnosisResult(List<Diagnosis> list)
        {
            Diagnosises = new List<SelectListItem>();

            if (list != null && list.Count > 0)
            {
                foreach (var diagnosis in list)
                {
                    Diagnosises.Add(new SelectListItem
                    {
                        Text = FormatText(diagnosis),
                        Value = diagnosis.Id.ToString()
                    });
                }
            }
            else
            {
                for (int i = 1; i <= 5; i++)
                {
                    Diagnosises.Add(new SelectListItem
                    {
                        Text = "Result " + i,
                        Value = i.ToString()
                    });
                }
            }
        }
        public int DiagnosisId { get; set; }
        public string DiagnosisName { get; set; }
        public string DiagnosisDescription { get; set; }
        public string DiagnosisTreatment { get; set; }
        public List<SelectListItem> Diagnosises { get; set; }
        public Wizard Wizard { get; set; }

        private string FormatText(Diagnosis diagnosis)
        {
            var result = diagnosis.Name;
            if(!string.IsNullOrEmpty(diagnosis.Description))
            {
                result += " - " + diagnosis.Description;
            }

            if (!string.IsNullOrEmpty(diagnosis.Treatment))
            {
                result += " - " + diagnosis.Treatment;
            }

            return result;
        }
    }
}