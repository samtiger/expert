﻿namespace ExpertSystem.Models
{
    public class Wizard
    {
        #region Patient
        public string PatientName { get; set; }
        public int PatientAge { get; set; }
        #endregion

        #region Syndromes
        public string[] Syndromes { get; set; }
        #endregion
    }
}