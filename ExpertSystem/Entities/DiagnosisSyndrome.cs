﻿namespace ExpertSystem.Entities
{
    public class DiagnosisSyndrome : BaseEntity
    {
        public int SyndromeId { get; set; }
        public int DiagnosisId { get; set; }
        public int Count { get; set; }
    }
}