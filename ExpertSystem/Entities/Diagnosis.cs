﻿using System;
using System.Collections.Generic;

namespace ExpertSystem.Entities
{
    public class Diagnosis : BasicEntity, IComparable<Diagnosis>
    {
        public string Description { get; set; }
        public string Treatment { get; set; }
        public List<Syndrome> Syndromes { get; set; }
        public List<DiagnosisSyndrome> DiagnosisSyndromes { get; set; }

        public int GetTotal()
        {
            var result = 0;
            foreach (var item in DiagnosisSyndromes)
            {
                result += item.Count;
            }
            return result;
        }

        public int CompareTo(Diagnosis other)
        {
            int ret = -1;
            if (Syndromes.Count < other.Syndromes.Count)
                ret = 1;
            else if (Syndromes.Count > other.Syndromes.Count)
                ret = -1;
            else if (Syndromes.Count == other.Syndromes.Count)
            {
                if (GetTotal() < other.GetTotal())
                    ret = 1;
                else if (GetTotal() > other.GetTotal())
                    ret = -1;
                else
                ret = 0;
            }
            return ret;
        }

    }
}