﻿namespace ExpertSystem.Entities
{
    public class BasicEntity : BaseEntity
    {
        public string Name { get; set; }
    }
}