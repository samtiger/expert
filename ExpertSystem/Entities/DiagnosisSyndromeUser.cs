﻿namespace ExpertSystem.Entities
{
    public class DiagnosisSyndromeUser : BaseEntity
    {
        public int DiagnosisId { get; set; }
        public int SyndromeId { get; set; }
        public int UserId { get; set; }
    }
}