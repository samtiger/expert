﻿namespace ExpertSystem.Entities
{
    public class Syndrome : BasicEntity
    {
        public string Description { get; set; }
    }
}