﻿using System;

namespace ExpertSystem.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}