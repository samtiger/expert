﻿using ExpertSystem.Entities;
using ExpertSystem.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ExpertSystem.Managers
{
    public class DatabaseManager
    {
        private string _connectionString = @"Data Source=FORLI\SQLEXPRESS;Initial Catalog=ExpertSystem;User ID=sa;Password=MVBVbk22+3xPzM";

        #region Helpers

        /// <summary>
        /// get the value from DataReader
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readerValue"></param>
        /// <param name="defaultValue"></param>
        /// <returns>T</returns>
        private T GetValue<T>(object readerValue, T defaultValue = default(T))
        {
            if (readerValue == null || readerValue == DBNull.Value)
                return defaultValue;
            else
                return (T)Convert.ChangeType(readerValue, typeof(T));
        }

        /// <summary>
        /// get the value from reader as enum
        /// </summary>
        private T GetEnumValue<T>(object readerValue, T defaultValue = default(T))
        {
            if (readerValue == null || readerValue == DBNull.Value)
                return defaultValue;
            else
                return (T)(readerValue);
        }

        /// <summary>
        /// sqlCommand execute non query
        /// </summary>
        private async Task ExecuteNonQuery(string storedProcedure, Action<SqlCommand> fillCommand)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    var command = new SqlCommand()
                    {
                        CommandType = System.Data.CommandType.StoredProcedure,
                        CommandText = storedProcedure,
                        Connection = connection
                    };
                    fillCommand(command);
                    await connection.OpenAsync();
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// sqlCommand execute scalar
        /// </summary>
        /// <returns>string</returns>
        private string ExecuteScalar(string storedProcedure, Action<SqlCommand> fillCommand)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    var command = new SqlCommand()
                    {
                        CommandType = System.Data.CommandType.StoredProcedure,
                        CommandText = storedProcedure,
                        Connection = connection
                    };
                    fillCommand(command);
                    connection.Open();
                    return (command.ExecuteScalar()).ToString();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// sqlCommand execute reader
        /// </summary>
        private async Task ExecuteReader(string storedProcedure, Action<SqlCommand> fillCommand, Action<SqlDataReader> fetchData)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    var command = new SqlCommand()
                    {
                        CommandType = System.Data.CommandType.StoredProcedure,
                        CommandText = storedProcedure,
                        Connection = connection
                    };
                    fillCommand(command);
                    await connection.OpenAsync();
                    fetchData(await command.ExecuteReaderAsync());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// fills base properties of any entity into the sql command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="model"></param>

        /// <summary>
        /// fills base properties of any entity into the sql command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="model"></param>

        /// <summary>
        /// fills nullable property into sql command (strings)
        /// </summary>
        /// <param name="command"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        private void FillNullable(SqlCommand command, string name, string value)
        {
            if (!string.IsNullOrEmpty(value))
                command.Parameters.AddWithValue(name, value);
        }

        /// <summary>
        /// fills nullable property into sql command (strings)
        /// </summary>
        /// <param name="command"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        private void FillNullable<T>(SqlCommand command, string name, T? value) where T : struct
        {
            if (value.HasValue)
            {
                command.Parameters.AddWithValue(name, value.Value);
            }
        }

        private int? GetNullableInteger(object v)
        {
            if (v != DBNull.Value)
                return Convert.ToInt32(v);
            return null;
        }

        #endregion

        public async Task<List<Diagnosis>> Process(Wizard request)
        {
            try
            {
                var response = new List<Diagnosis>();

                var syndromeList = new List<Syndrome>();
                var diagnosisList = new List<Diagnosis>();
                var diagnosisSyndromeList = new List<DiagnosisSyndrome>();

                await ExecuteReader("Process_Diagnosis", delegate (SqlCommand cmd)
                {
                    DataTable syndromes = new DataTable();
                    syndromes.Columns.Add("Name", typeof(string));

                    foreach (var item in request.Syndromes)
                    {
                        if (!string.IsNullOrEmpty(item))
                            syndromes.Rows.Add(item);
                    }

                    cmd.Parameters.AddWithValue("@Syndromes", SqlDbType.Structured).Value = syndromes;

                },
                async delegate (SqlDataReader reader)
                {
                    while (await reader.ReadAsync())
                    {
                        syndromeList.Add(new Syndrome
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Name = reader["Name"].ToString()
                        });
                    }

                    if (await reader.NextResultAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            diagnosisList.Add(new Diagnosis
                            {
                                Id = Convert.ToInt32(reader["Id"]),
                                Name = reader["Name"].ToString(),
                                Description = reader["Description"] != DBNull.Value ? reader["Description"].ToString() : "",
                                Treatment = reader["Treatment"] != DBNull.Value ? reader["Treatment"].ToString() : "",
                            });
                        }
                    }

                    if (await reader.NextResultAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            diagnosisSyndromeList.Add(new DiagnosisSyndrome
                            {
                                Id = Convert.ToInt32(reader["Id"]),
                                DiagnosisId = Convert.ToInt32(reader["DiagnosisId"]),
                                SyndromeId = Convert.ToInt32(reader["SyndromeId"]),
                                Count = Convert.ToInt32(reader["Count"])
                            });
                        }
                    }

                });

                foreach (var diangosis in diagnosisList)
                {
                    diangosis.Syndromes = GetSyndromes(diangosis.Id, syndromeList, diagnosisSyndromeList);
                    diangosis.DiagnosisSyndromes = diagnosisSyndromeList.Where(m => m.DiagnosisId == diangosis.Id).ToList();
                }
                var diagnosisArray = diagnosisList.ToArray();

                Array.Sort(diagnosisArray);
                return diagnosisArray.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private List<Syndrome> GetSyndromes(int id, List<Syndrome> syndromeList, List<DiagnosisSyndrome> diagnosisSyndromeList)
        {
            return syndromeList.Where(m => diagnosisSyndromeList.FirstOrDefault(e => e.SyndromeId == m.Id && e.DiagnosisId == id) != null).ToList();
        }

        public async Task<List<Diagnosis>> Save(DiagnosisResult request)
        {
            try
            {
                var response = new List<Diagnosis>();

                await ExecuteNonQuery("Save_Diagnosis", delegate (SqlCommand cmd)
                {
                    cmd.Parameters.AddWithValue("@DiagnosisId", request.DiagnosisId);
                    cmd.Parameters.AddWithValue("@DiagnosisName", request.DiagnosisName);
                    cmd.Parameters.AddWithValue("@DiagnosisDescription", request.DiagnosisDescription);
                    cmd.Parameters.AddWithValue("@DiagnosisTreatment", request.DiagnosisTreatment);

                    cmd.Parameters.AddWithValue("@PatientName", request.Wizard.PatientName);
                    cmd.Parameters.AddWithValue("@PatientAge", request.Wizard.PatientAge);

                    DataTable syndromes = new DataTable();
                    syndromes.Columns.Add("Name", typeof(string));

                    foreach (var item in request.Wizard.Syndromes)
                    {
                        if (!string.IsNullOrEmpty(item))
                            syndromes.Rows.Add(item);
                    }

                    cmd.Parameters.AddWithValue("@Syndromes", SqlDbType.Structured).Value = syndromes;

                });
                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<Syndrome>> ListSyndromes()
        {
            try
            {
                var response = new List<Syndrome>();

                await ExecuteReader("List_Syndromes", delegate (SqlCommand cmd)
                {
                },
                async delegate (SqlDataReader reader)
                {
                    while (await reader.ReadAsync())
                    {
                        response.Add(new Syndrome
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Name = reader["Name"].ToString()
                        });
                    }
                });
                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}