﻿using ExpertSystem.Entities;
using ExpertSystem.Managers;
using ExpertSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ExpertSystem.Controllers
{
    public class HomeController : Controller
    {
        DatabaseManager _manager = new DatabaseManager();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(Wizard model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                var response = await _manager.Process(model);

                Session["wizard"] = model;
                Session["diagnosis"] = response;

                return RedirectToAction("Result");
            }
            catch (Exception)
            {
                return View(model);
            }
        }

        [HttpPost]
        public async Task<ActionResult> ProcessApi(Wizard request)
        {
            try
            {
                var response = await _manager.Process(request);

                return Json(response.Select(m=> new { m.Id, m.Name, m.Description, m.Treatment}));
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> SubmitApi(SubmitModel request)
        {
            try
            {
                var response = await _manager.Save(new DiagnosisResult
                {
                    Wizard = new Wizard
                    {
                        PatientAge = request.PatientAge,
                        PatientName = request.PatientName,
                        Syndromes = request.Syndromes
                    },
                    DiagnosisId = request.DiagnosisId,
                    DiagnosisDescription = request.DiagnosisDescription,
                    DiagnosisTreatment = request.DiagnosisTreatment,
                    DiagnosisName = request.DiagnosisName
                });

                return Json("Worked");
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        public ActionResult Result()
        {
            return View(new DiagnosisResult(Session["diagnosis"] as List<Diagnosis>));
        }

        [HttpPost]
        public async Task<ActionResult> Result(DiagnosisResult model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(new DiagnosisResult(Session["diagnosis"] as List<Diagnosis>));
                }

                model.Wizard = Session["wizard"] as Wizard;
                await _manager.Save(model);
                
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View(new DiagnosisResult(Session["diagnosis"] as List<Diagnosis>));
            }
        }

        public async Task<ActionResult> List(string search)
        {
            var list = await _manager.ListSyndromes();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

    }
}